/**
 * Extend the base Actor class to implement additional logic specialized for Pathfinder.
 */
class ActorPF extends Actor {


  updateOwnedItem(itemData, push=false) {
  
  // Perform the parent update
  
  
  // If we updated an equipped item - update the character AC
    if (itemData["data.equipped.value"] == true) {
  //Get total AC value from equipped equipment.
        let isEquipped = this.data.items.filter(i => i.type === "equipment" && i.data.equipped.value == true);
        let ac = isEquipped.map(i => i.data.armor.value);
        let sum = ac.reduce((pv, cv) => pv+cv, 0);
        this.update({"data.attributes.equip_ac.value": sum}, false);
                                                }
  return super.updateOwnedItem(itemData, push);
  }

  deleteOwnedItem(itemId, push=false) {
    super.deleteOwnedItem(itemId, push);
      //Get total AC value from equipped equipment.
            let isEquipped = this.data.items.filter(i => i.type === "equipment" && i.data.equipped.value == true);
            let ac = isEquipped.map(i => i.data.armor.value);
            let sum = ac.reduce((pv, cv) => pv+cv, 0);
            this.update({"data.attributes.equip_ac.value": sum}, false);
                                                    

  }


  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData(actorData) {
    actorData = super.prepareData(actorData);
    const data = actorData.data;

    // Prepare Character data
    if ( actorData.type === "character" ) this._prepareCharacterData(actorData);
    else if ( actorData.type === "npc" ) this._prepareNPCData(actorData);

    // Ability modifiers and saves
    for (let abl of Object.values(data.abilities)) {
      abl.mod = Math.floor((abl.value - 10) / 2);
      abl.save = abl.mod + ((abl.proficient || 0) * data.attributes.prof.value);
    }

    // Skill modifiers
    for (let skl of Object.values(data.skills)) {
      skl.value = parseFloat(skl.value || 0);
      skl.ranks = parseInt(skl.ranks || 0);
      skl.mod = data.abilities[skl.ability].mod + skl.ranks + (skl.ranks > 0 ? skl.value : 0);
    }

    // Attributes
    //if ( actorData.type === "character" ) {
    data.attributes.fort.mod = data.abilities.con.mod + (data.attributes.fort.value || 0);
    data.attributes.reflex.mod = data.abilities.dex.mod + (data.attributes.reflex.value || 0);
    data.attributes.will.mod = data.abilities.wis.mod + (data.attributes.will.value || 0);
    data.attributes.flat_ac.value = (data.attributes.flat_ac.value || 0);
    data.attributes.equip_ac.value = (data.attributes.equip_ac.value || 0);
    data.attributes.natural_ac.value = parseInt((data.attributes.natural_ac.value || 0));
    data.attributes.size_ac.value = (data.attributes.size_ac.value || 0);
    data.attributes.dodge_ac.value = (data.attributes.dodge_ac.value || 0);
    data.attributes.deflect_ac.value = (data.attributes.deflect_ac.value || 0);
    data.attributes.misc_ac.value = (data.attributes.misc_ac.value || 0);
    data.attributes.temp_hp.value = (data.attributes.temp_hp.value || 0);
    data.attributes.touch_ac.min = (parseInt(10 + data.abilities.dex.mod + data.attributes.size_ac.value + data.attributes.dodge_ac.value + data.attributes.deflect_ac.value + data.attributes.misc_ac.value) || 10);
    data.attributes.ac.value = (parseInt(10 + data.abilities.dex.mod + data.attributes.natural_ac.value + data.attributes.size_ac.value + data.attributes.dodge_ac.value + data.attributes.deflect_ac.value + data.attributes.misc_ac.value + data.attributes.equip_ac.value) || data.attributes.touch_ac.min);
    data.attributes.flat_ac.value = 10 + parseInt(data.attributes.equip_ac.value + data.attributes.natural_ac.value + data.attributes.size_ac.value + data.attributes.deflect_ac.value);
    if (data.abilities.dex.mod < 0) { data.attributes.flat_ac.value = data.attributes.flat_ac.value + data.abilities.dex.mod; }
    data.attributes.cmb.mod = data.abilities.str.mod + (data.attributes.bab.value || 0);
    //}
    data.attributes.init.mod = data.abilities.dex.mod + (data.attributes.init.value || 0);
    data.attributes.spelldc.value = 8 + data.attributes.prof.value + data.abilities.int.mod;

    // Spell DC
    let spellAbl = data.attributes.spellcasting.value || "int";
    data.attributes.spelldc.value = 8 + data.attributes.prof.value + data.abilities[spellAbl].mod;
    
    // Return the prepared Actor data
    return actorData;
  }

  /* -------------------------------------------- */

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;
    // Level, experience, and proficiency
    data.details.level.value = parseInt(data.details.level.value);
    data.details.xp.max = this.getLevelExp(data.details.level.value || 1);
    let prior = this.getLevelExp(data.details.level.value - 1 || 0),
          req = data.details.xp.max - prior;
    data.details.xp.pct = Math.min(Math.round((data.details.xp.value -prior) * 100 / req), 99.5);
    //Get total AC value from equipped equipment.
    let isEquipped = actorData.items.filter(i => i.type === "equipment" && i.data.equipped.value == true);
    let ac = isEquipped.map(i => i.data.armor.value);
    let sum = ac.reduce((pv, cv) => pv+cv, 0);
    data.attributes.equip_ac.value = sum;
  }

  /* -------------------------------------------- */

  /**
   * Prepare NPC type specific data
   */
  _prepareNPCData(actorData) {
    const data = actorData.data;
    // CR, kill exp, and proficiency
    data.details.cr.value = parseFloat(data.details.cr.value) || 0;
    data.details.xp.value = this.getCRExp(data.details.cr.value);
    data.attributes.prof.value = Math.floor((data.details.cr.value + 7) / 4);
    let isEquipped = actorData.items.filter(i => i.type === "equipment" && i.data.equipped.value == true);
    let ac = isEquipped.map(i => i.data.armor.value);
    let sum = ac.reduce((pv, cv) => pv+cv, 0);
    data.attributes.equip_ac.value = sum;
  }

  /* -------------------------------------------- */

  /**
   * Return the amount of experience required to gain a certain character level.
   * @param level {Number}  The desired level
   * @return {Number}       The XP required
   */
  getLevelExp(level) {
    const levels = [0, 2000, 5000, 9000, 15000, 23000, 35000, 51000, 75000, 105000, 155000,
      220000, 315000, 445000, 635000, 890000, 1300000, 1800000, 2250000, 3600000];
    return levels[Math.min(level, levels.length - 1)];
  }

  /* -------------------------------------------- */

  /**
   * Return the amount of experience granted by killing a creature of a certain CR.
   * @param cr {Number}     The creature's challenge rating
   * @return {Number}       The amount of experience granted per kill
   */
  getCRExp(cr) {
    if (cr < 1.0) return Math.max(400 * cr, 10);
    const xps = [10, 400, 600, 800, 1200, 1600, 2400, 3200, 4800, 6400, 9600, 12800, 19200, 25600,
        38400, 51200, 76800, 102400, 153600, 204800, 307200, 409600, 614400, 819200, 1228800, 1638400];
    return xps[cr];
  }

  /* -------------------------------------------- */

  get _skillSaveRollModalHTML() {
    return `
    <form>
        <div class="form-group">
            <label>Formula</label>
            <input type="text" name="formula" value="1d20 + @mod + @bonus" disabled/>
        </div>
        <div class="form-group">
            <label>Situational Bonus?</label>
            <input type="text" name="bonus" value="" placeholder="e.g. +1d4"/>
        </div>
    </form>
    `;
  }

  /* -------------------------------------------- */

  /**
   * Roll a Skill Check
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param skill {String}    The skill id
   */
  rollSkill(skillName) {
    let skl = this.data.data.skills[skillName],
      abl = this.data.data.abilities[skl.ability],
      parts = ["1d20"],
      flavor = `${skl.label} Skill Check`;
      if (skl.mod !== 0) parts.push("@mod");

    // Create a dialog
    new Dialog({
      title: `${skl.label} (${abl.label}) Skill Check`,
      content: this._skillSaveRollModalHTML,
      buttons: {
        normal: {
          label: "Roll"
        }
      },
      close: html => {
        let bonus = (parseInt(html.find('[name="bonus"]').val()) || 0);
        if (bonus !== 0) parts.push("@bonus");
        new Roll(parts.join(" + "), {mod: skl.mod, bonus: bonus}).toMessage({
          alias: this.name,
          flavor: flavor,
          sound: "sounds/dice.wav"
        });
      }
    }).render(true);
  }

  // Edit a skill

  editSkill(skillName, actor) {
    let skl = this.data.data.skills[skillName],
    abl = this.data.data.abilities;
    let ablSelect = "";
    Object.keys(this.data.data.abilities).forEach(function(key) {
      let ablLabel = abl[key].label;
      if (skl.ability == key) {
        ablSelect = ablSelect + `<option value="${key}" selected="selected">${ablLabel}</option>`;  
      } else {
      ablSelect = ablSelect + `<option value="${key}">${ablLabel}</option>`;
      }
  });

    // Create a dialog
    new Dialog({
      title: `Edit ${skl.label} Skill`,
      content: `
      
    <form>
    <div style="text-align: left;">
    <b>Skill Name:</b> <input style="background: none; border: none;" type="text" name="skillname" value="${skl.label}"/>
    <b>Ability:</b> <select style="height: 20px;" name="ability" data-dtype="{{data.ability.type}}">
    ${ablSelect}
    </select>
    </div>
    </form>
    `,
      buttons: {
        normal: {
          label: "Save Changes"
        }
      },
      close: html => {
        //Actual Skill Save Code
      let new_skill_name = html.find('[name="skillname"]').val();
      let new_skill_ability = html.find('[name="ability"]').val();
      let sklupd = 'data.skills.' + skillName + '.label';
      let sklupda = 'data.skills.' + skillName + '.ability';
      console.log(sklupd)
      actor.update({
        [sklupd]: new_skill_name,
        [sklupda]: new_skill_ability
      }, true);
      }
    }).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Roll a generic ability test or saving throw.
   * Prompt the user for input on which variety of roll they want to do.
   * @param abilityId {String}    The ability id (e.g. "str")
   */
  rollAbility(abilityId) {
    let abl = this.data.data.abilities[abilityId];
    new Dialog({
      title: `${abl.label} Ability Check`,
      content: `<p>What type of ${abl.label} check?</p>`,
      buttons: {
        test: {
          label: "Ability Test",
          callback: () => this.rollAbilityTest(abilityId)
        },
        save: {
          label: "Saving Throw",
          callback: () => this.rollAbilitySave(abilityId)
        }
      }
    }).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Roll an Ability Test
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param abilityId {String}    The ability ID (e.g. "str")
   */
  rollAbilityTest(abilityId) {
    let abl = this.data.data.abilities[abilityId],
        parts = ["1d20"],
        flavor = `${abl.label} Ability Test`;
        if (abl.mod !== 0) parts.push("@mod");

    // Create a dialog
    new Dialog({
      title: flavor,
      content: this._skillSaveRollModalHTML,
      buttons: {
        normal: {
          label: "Roll"
        }
      },
      close: html => {
        let bonus = (parseInt(html.find('[name="bonus"]').val()) || 0);
        if (bonus !== 0) parts.push("@bonus");
        new Roll(parts.join(" + "), {mod: abl.mod, bonus: bonus}).toMessage({
          alias: this.name,
          flavor: flavor,
          sound: "sounds/dice.wav"
        });
      }
    }).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Roll a saving Saving Throw
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param saveName {String}    The save type (e.g. "fort")
   */
  rollSave(saveName) {
    let mod = (this.data.data.attributes[saveName].mod || 0),
      parts = ["1d20"],
      saveLabel = this.data.data.attributes[saveName].label,
      flavor = `${saveLabel} Save Roll`;
      
    // Create the dialog content
    let content = `
    <form>
        <div class="form-group">
            <label>Formula</label>
            <input type="text" name="formula" value="1d20 + @mod + @bonus" disabled/>
        </div>
        <div class="form-group">
            <label>Situational Bonus?</label>
            <input type="text" name="bonus" value="" placeholder="e.g. +5"/>
        </div>
    </form>
    `;
    if (mod !== 0) parts.push("@mod");
    // Create a dialog
    new Dialog({
      title: `${saveLabel} Save Check`,
      content: content,
      buttons: {
        normal: {
          label: "Roll",
        }
      },
      close: html => {
        let bonus = (parseInt(html.find('[name="bonus"]').val()) || 0);
        if (bonus !== 0) parts.push("@bonus");
        new Roll(parts.join(" + "), {mod: mod, bonus: bonus}).toMessage({
          alias: this.name,
          flavor: flavor,
          sound: "sounds/dice.wav"
        });
      }
    }).render(true);
  }
}


/* -------------------------------------------- */
/*  Actor Character Sheet                       */
/* -------------------------------------------- */

/**
 * Extend the basic ActorSheet class to do all the Pathfinder things!
 */
class ActorPFSheet extends ActorSheet {

  /**
   * The actor sheet template comes packaged with the system
   */
  get template() {
    const path = "public/systems/pathfinder/templates/actors/";
    if ( this.actor.data.type === "character" ) return path + "actor-sheet.html";
    else if ( this.actor.data.type === "npc" ) return path + "npc-sheet.html";
    else throw "Unrecognized Actor type " + this.actor.data.type;
  }

  /* -------------------------------------------- */

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   */
  getData() {
    const sheetData = super.getData();
    
    // Level and CR
    if ( sheetData.actor.type === "npc" ) {
      let cr = sheetData.data.details.cr;
      let crs = {0: "0", 0.125: "1/8", 0.25: "1/4", 0.5: "1/2"};
      cr.str = (cr.value >= 1) ? String(cr.value) : crs[cr.value] || 0;
    }

    // Ability proficiency
    for ( let abl of Object.values(sheetData.data.abilities)) {
      abl.icon = this._getProficiencyIcon(abl.proficient);
      abl.hover = this._getProficiencyHover(abl.proficient);
    }

    // Update skill labels
    for ( let skl of Object.values(sheetData.data.skills)) {
      skl.ability = sheetData.data.abilities[skl.ability].label.substring(0, 3);
      skl.icon = this._getProficiencyIcon(skl.value);
      skl.hover = this._getProficiencyHover(skl.value);
    }

    // Prepare owned items
    this._prepareItems(sheetData.actor);

    // Return data to the sheet
    return sheetData;
  }

  /* -------------------------------------------- */

  _prepareItems(actorData) {

    // Inventory
    const inventory = {
      weapon: { label: "Weapons", items: [] },
      equipment: { label: "Equipment", items: [] },
      consumable: { label: "Consumables", items: [] },
      goods: { label: "Goods", items: [] },
      misc: { label: "Misc", items: [] },
    };

    // Spellbook
    const spellbook = {};
    // Feats
    const feats = [];

    // Classes
    const classes = [];

    // Iterate through items, allocating to containers
    let totalWeight = 0;
    for ( let i of actorData.items ) {
      i.img = i.img || DEFAULT_TOKEN;

      // Inventory
      if ( Object.keys(inventory).includes(i.type) ) {
        i.data.quantity.value = i.data.quantity.value || 1;
        i.data.weight.value = i.data.weight.value || 0;
        i.totalWeight = Math.round(i.data.quantity.value * i.data.weight.value * 10) / 10;
        i.hasCharges = (i.type === "consumable") && i.data.charges.max > 0;
        inventory[i.type].items.push(i);
        totalWeight += i.totalWeight;
      }

      // Spells
      else if ( i.type === "spell" ) {
        let lvl = i.data.level.value || 0;
        spellbook[lvl] = spellbook[lvl] || {
          isCantrip: lvl === 0,
          label: CONFIG.spellLevels[lvl],
          spells: [],
          uses: actorData.data.spells["spell"+lvl].value || 0,
          slots: actorData.data.spells["spell"+lvl].max || 0
        };
        i.data.school.str = CONFIG.spellSchools[i.data.school.value];
        spellbook[lvl].spells.push(i);
      }

      // Class
      else if ( i.type === "class" ) {
        classes.push(i);
        classes.sort((a, b) => b.levels > a.levels);
      }

      // Feats
      else if ( i.type === "feat" ) feats.push(i);
    }
    
    // Assign and return
    actorData.inventory = inventory;
    actorData.spellbook = spellbook;
    actorData.feats = feats;
    actorData.classes = classes;

    // Inventory encumbrance
    //TODO Pathfinder
    let enc = {
      max: Math.round((100 * (4**(0.1 * (actorData.data.abilities.str.value - 10))))/5)*5,
      value: Math.round(totalWeight * 10) / 10,
    };
    enc.pct = Math.min(enc.value * 100 / enc.max, 99);
    actorData.data.attributes.encumbrance = enc;
  }

  /* -------------------------------------------- */

  _cycleSkillProficiency(level) {
    const levels = [0, 3];
    let idx = levels.indexOf(level);
    return levels[(idx === levels.length - 1) ? 0 : idx + 1]
  }

  /* -------------------------------------------- */

    /**
   * Get the font-awesome icon used to display a certain level of skill proficiency
   * @private
   */
  _getProficiencyIcon(level) {
    const icons = {
      0: '<i class="far fa-circle"></i>',
      3: '<i class="fas fa-check"></i>'
      };
    return icons[level];
  }

  /* -------------------------------------------- */

  /**
   * Get the hover text used to display a certain level of skill proficiency
   * @private
   */
  _getProficiencyHover(level) {
    return {
      0: "Non Class Skill",
      3: "Class Skill"
    }[level];
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
	  super.activateListeners(html);

	  // Pad field width
    html.find('[data-wpad]').each((i, e) => {
      let text = e.tagName === "INPUT" ? e.value : e.innerText,
          w = text.length * parseInt(e.getAttribute("data-wpad")) / 2;
      e.setAttribute("style", "flex: 0 0 " + w + "px");
    });

    // Activate tabs
    html.find('.tabs').each((_, el) => {
      let tabs = $(el),
          initial = this.actor.data.flags["_sheetTab-" + tabs.attr("data-tab-container")];
      new Tabs(tabs, initial, clicked => {
        this.actor.data.flags["_sheetTab-" + clicked.parent().attr("data-tab-container")] = clicked.attr("data-tab");
      });
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

	  // Activate TinyMCE Editors
	  html.find(".editor a.editor-edit").click(ev => {
	    let button = $(ev.currentTarget),
	        editor = button.siblings(".editor-content");
	    let mce = createEditor({
        target: editor[0],
        height: editor.parent().height() - 40,
        save_enablewhendirty: true,
        save_onsavecallback: ed => {
          let target = editor.attr("data-edit");
          this.actor.update({[target]: ed.getContent()}, true);
          ed.remove();
          ed.destroy();
        }
      }).then(ed => {
        this.mce = ed[0];
        button.hide();
        ed[0].focus();
      });
    });

    /* -------------------------------------------- */
    /*  Edit Armor Class Bonuses
    /* -------------------------------------------- */
    html.find('h3.ac-edit').click(ev => {
      new Dialog({
        title: `AC Bonuses for ${this.actor.name}`,
        content: `
                  <div class="flexrow attribute-name" style="text-align: center;">
                  <div class="flexcol">
                    Natural AC
                  </div>
                  <div class="flexcol">
                    Size
                  </div>
                  <div class="flexcol">
                    Dodge
                  </div>
                  <div class="flexcol">
                  Deflect
                  </div>
                  <div class="flexcol">
                  Misc.
                  </div>
                  </div>
                  <div class="flexrow attribute-value" style="text-align: center;">
                  <div class="flexcol">
                    <input style="width: 94px;" type="text" name="natural_ac" data-dtype="${this.actor.data.data.attributes.natural_ac.type}" value="${this.actor.data.data.attributes.natural_ac.value}" placeholder="0" autocomplete="off"/>
                  </div>
                  <div class="flexcol">
                    <input style="width: 94px;" type="text" name="size_ac" data-dtype="${this.actor.data.data.attributes.size_ac.type}" value="${this.actor.data.data.attributes.size_ac.value}" placeholder="0" autocomplete="off"/>
                  </div>
                  <div class="flexcol">
                    <input style="width: 94px;" type="text" name="dodge_ac" data-dtype="${this.actor.data.data.attributes.dodge_ac.type}" value="${this.actor.data.data.attributes.dodge_ac.value}" placeholder="0" autocomplete="off"/>
                  </div>
                  <div class="flexcol">
                    <input style="width: 94px;" type="text" name="deflect_ac" data-dtype="${this.actor.data.data.attributes.deflect_ac.type}" value="${this.actor.data.data.attributes.deflect_ac.value}" placeholder="0" autocomplete="off"/>
                  </div>
                  <div class="flexcol">
                    <input style="width: 94px;" type="text" name="misc_ac" data-dtype="${this.actor.data.data.attributes.misc_ac.type}" value="${this.actor.data.data.attributes.misc_ac.value}" placeholder="0" autocomplete="off"/>
                  </div></div>`,
        buttons: {
          save: {
            icon: '<i class="fas fa-save"></i>',
            label: "Save",
            callback: html => this.actor.update(
              {
                'data.attributes.natural_ac.value': parseInt(html.find('input[name="natural_ac"]').val()),
                'data.attributes.size_ac.value': parseInt(html.find('input[name="size_ac"]').val()),
                'data.attributes.dodge_ac.value': parseInt(html.find('input[name="dodge_ac"]').val()),
                'data.attributes.deflect_ac.value': parseInt(html.find('input[name="deflect_ac"]').val()),
                'data.attributes.misc_ac.value': parseInt(html.find('input[name="misc_ac"]').val())
              
              }, true)
          },
        }
      }).render(true);
    });

    /* -------------------------------------------- */
    /*  Abilities and Skills
    /* -------------------------------------------- */

    // Ability Checks
    html.find('.ability-name').click(ev => {
      let abl = ev.currentTarget.parentElement.getAttribute("data-ability");
      this.actor.rollAbility(abl);
    });

    // Toggle Skill Proficiency
    html.find('.skill-proficiency').click(ev => {
      let field = $(ev.currentTarget).siblings('input[type="hidden"]');
      field.val(this._cycleSkillProficiency(parseFloat(field.val())));
      let formData = validateForm(field.parents('form')[0]);
      this.actor.update(formData, true);
    });

    // Roll Skill Checks
    html.find('.skill-name').click(ev => {
      let skl = ev.currentTarget.parentElement.getAttribute("data-skill");
      this.actor.rollSkill(skl);
    });
    // Edit Skill
    html.find('h3.skill-name').contextmenu(ev => {
      let skl = ev.currentTarget.parentElement.getAttribute("data-skill");
      this.actor.editSkill(skl,this.actor);
    });
    // Roll Skill Checks
    html.find('div.save-roll').click(ev => {
      let save = ev.currentTarget.parentElement.getAttribute("save-name");
      this.actor.rollSave(save);
        });

    /* -------------------------------------------- */
    /*  Resource Tracker
    /* -------------------------------------------- */

    html.find('.res-add').click(ev => {
      let res_add = (html.find('[name="data.resources.primary.value"]').val() || 0 );
      let set_res = parseInt(res_add) + 1;
      html.find('[name="data.resources.primary.value"]').val(set_res);
      let field = $(ev.currentTarget).siblings(html.find('[name="data.resources.primary.value"]'));
      let formData = validateForm(field.parents('form')[0]);
      this.actor.update(formData, true);
    });
    
    html.find('.res-sub').click(ev => {
      let res_sub = html.find('[name="data.resources.primary.value"]').val();
      let set_res = parseInt(res_sub) - 1;
      html.find('[name="data.resources.primary.value"]').val(set_res);
      let field = $(ev.currentTarget).siblings(html.find('[name="data.resources.primary.value"]'));
      let formData = validateForm(field.parents('form')[0]);
      this.actor.update(formData, true);
    });
    /* -------------------------------------------- */
    /*  Rollable Items                              */
    /* -------------------------------------------- */

    html.find('.item .rollable').click(ev => {
      let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id")),
        Item = CONFIG.Item.entityClass,
        item = new Item(this.actor.items.find(i => i.id === itemId), this.actor);
      item.roll();
    });

    /* -------------------------------------------- */
    /*  Inventory
    /* -------------------------------------------- */

    // Create New Item
    html.find('.item-create').click(ev => {
      let type = ev.currentTarget.getAttribute("data-item-type");
      this.actor.createOwnedItem({name: "New " + type.capitalize(), type: type}, true, {renderSheet: true});
    });

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id"));
      let Item = CONFIG.Item.entityClass;
      const item = new Item(this.actor.items.find(i => i.id === itemId), this.actor);
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      let li = $(ev.currentTarget).parents(".item"),
          itemId = Number(li.attr("data-item-id"));
      this.actor.deleteOwnedItem(itemId, true);
      li.slideUp(200, () => this.render(false));
    });

    /* -------------------------------------------- */
    /*  Miscellaneous
    /* -------------------------------------------- */

    html.find('.npc-roll-hp').click(ev => {
      let ad = this.actor.data.data;
      let hp = new Roll(ad.attributes.hp.formula).roll().total;
      Audio.play({src: CONFIG.sounds.dice, volume: 0.8});
      this.actor.update({"data.attributes.hp.value": hp, "data.attributes.hp.max": hp}, true);
    });

    /* Item Dragging */
    let handler = ev => this._onDragItemStart(ev);
    html.find('.item').each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });
  }

  /* -------------------------------------------- */

  /**
   * Customize form submission for Pathfinder actor sheets
   * @private
   */
  _onSubmit(event) {

        // Save MCE editor content
        if ( this.mce ) {
          const content = this.mce.getContent();
          this.element.find('[data-edit="data.details.biography.value"]').html(content);
        }

    // NPC Challenge Rating
    if (this.actor.data.type === "npc") {
      let form = $(event.currentTarget),
        cr = form.find('.level input'),
        val = cr.val(),
        crs = {"1/8": 0.125, "1/4": 0.25, "1/2": 0.5};
      cr.val(crs[val] || val);
    }

    // Parent submission steps
    super._onSubmit(event);
  }

    /* -------------------------------------------- */

    _onDragItemStart(event) {
      let itemId = Number(event.currentTarget.getAttribute("data-item-id"));
      event.dataTransfer.setData("text/plain", JSON.stringify({
        type: "Item",
        actorId: this.actor._id,
        id: itemId
      }));
    }
}


/* -------------------------------------------- */


CONFIG.Actor.entityClass = ActorPF;
CONFIG.Actor.sheetClass = ActorPFSheet;
CONFIG.ActorPFSheet = {
  "width": 720,
  "height": 800
};


/* -------------------------------------------- */
